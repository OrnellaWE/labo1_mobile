package com.example.labo1;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.content.Intent;
import android.app.AlertDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Button;
import android.content.SharedPreferences;
import android.widget.Toast;

import java.net.URLEncoder;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;



public class MainActivity extends AppCompatActivity {

private EditText txtuser;
private EditText txtpw;
private Button  log;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txtuser = (EditText) findViewById(R.id.usr);
        txtpw = (EditText) findViewById(R.id.pw);
        log = (Button) findViewById(R.id.logUserIn);
    }



        public void logUserIn (View v){
            String userN = this.txtuser.getText().toString();
            String userp = this.txtpw.getText().toString();

             class db extends AsyncTask {
                private Context c;
                private android.app.AlertDialog ad;

                public db(Context c){
                    this.c = c;
                }
                @Override
                protected void onPreExecute() {
                    this.ad = new AlertDialog.Builder(this.c).create();
                    this.ad.setTitle("Login Status");

                }

                @Override
                protected void onPostExecute(Object o) {
                    this.ad.setMessage((String)o);
                    this.ad.show();
                }


                @Override
                protected Object doInBackground(Object[] param) {
                    String cible = "http://192.168.0.102/Android/connexion.php";
                    try {
                        URL url = new URL(cible);
                        HttpURLConnection con = (HttpURLConnection)url.openConnection();
                        con.setDoInput(true);
                        con.setDoOutput(true);
                        con.setRequestMethod("POST");

                        OutputStream outs = con.getOutputStream();
                        BufferedWriter bufw = new BufferedWriter((new OutputStreamWriter(outs,"utf-8")));
                        String msg = URLEncoder.encode("user","utf-8")+"="+
                                URLEncoder.encode((String)param[0],"utf8")+
                                "&"+URLEncoder.encode("pw","utf-8")+"="+
                                URLEncoder.encode((String)param[1],"utf8");

                        bufw.write(msg);
                        bufw.flush();
                        bufw.close();
                        outs.close();

                        InputStream ins = con.getInputStream();
                        BufferedReader bufr = new BufferedReader(new InputStreamReader(ins,"iso-8859-1"));
                        String line;
                        StringBuffer sbuff = new StringBuffer();

                        while ((line = bufr.readLine())!=null){
                            sbuff.append(line+"\n");
                        }
                        return  sbuff.toString();


                    }
                    catch(Exception e){
                        return  e.getMessage();

                    }
                }
            }

            db dbw = new db(this);
            dbw.execute(userN, userp);
            Intent i = new Intent(this, Menu.class);
            startActivity(i);
            Toast.makeText(this,"Welcome "+this.txtuser.getText(),Toast.LENGTH_LONG).show();

        }
    public void sign(View view){

        startActivity(new Intent(this, inscription.class));
    }


}

