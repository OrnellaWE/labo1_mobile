package com.example.labo1;
import android.app.Activity;
import androidx.appcompat.app.AppCompatActivity;
import android.content.Context;
import android.os.AsyncTask;
import android.content.Intent;
import android.content.SharedPreferences;
import android.app.AlertDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Button;
import android.widget.Toast;

import java.net.URLEncoder;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
public class Menu extends AppCompatActivity{
    private Button  update;
    private Button  check;
    private Button  participe;

    private EditText txtnom;
    private EditText txtprenom;
    private EditText txtuser;
    private EditText txtpw;
    private EditText txtolduser;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu);
        update = (Button) findViewById(R.id.update);
        check = (Button) findViewById(R.id.check);
        participe = (Button) findViewById(R.id.participe);

        txtnom=(EditText)findViewById(R.id.nom);
        txtprenom=(EditText)findViewById(R.id.prenom);

        txtuser = (EditText) findViewById(R.id.usr);
        txtpw = (EditText) findViewById(R.id.pw);
        txtolduser=(EditText) findViewById(R.id.oldusr);
    }


    class dbWorker extends AsyncTask {

        private Context c;
        private AlertDialog ad;

        public dbWorker(Context c) {
            this.c = c;
        }

        @Override
        protected void onPreExecute() {
            this.ad = new AlertDialog.Builder(this.c).create();
            this.ad.setTitle("Updating");
        }


        @Override
        protected Object doInBackground(Object[] param) {
            String cible = "http://192.168.0.102/Android/modification.php";
            try {
                URL url = new URL(cible);
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                con.setDoInput(true);
                con.setDoOutput(true);
                con.setRequestMethod("POST");

                OutputStream outs = con.getOutputStream();
                BufferedWriter bufw = new BufferedWriter(new OutputStreamWriter(outs, "utf-8"));
                String msg = URLEncoder.encode("nom","utf-8")+"="+
                        URLEncoder.encode((String)param[0], "utf-8")+"&"+
                        URLEncoder.encode( "prenom", "utf-8")+"="+
                        URLEncoder.encode((String)param[1], "utf-8")+"&"+
                        URLEncoder.encode("user", "utf-8")+"="+
                        URLEncoder.encode((String)param[2], "utf-8")+"&"+
                        URLEncoder.encode("pw", "utf-8")+"="+
                        URLEncoder.encode((String)param[3], "utf-8")+"&"+
                        URLEncoder.encode("olduser", "utf-8")+"="+
                        URLEncoder.encode((String)param[4], "utf-8");

                bufw.write(msg);
                bufw.flush();
                bufw.close();
                outs.close();

                InputStream ins = con.getInputStream();
                BufferedReader bufr = new BufferedReader(new InputStreamReader(ins, "iso-8859-1"));
                String line;
                StringBuffer sbuff = new StringBuffer();

                while ((line = bufr.readLine())!=null){
                    sbuff.append(line+"\n");
                }
                return  sbuff.toString();



            } catch (Exception ex) {
                return ex.getMessage();
            }


        }


        @Override
        protected void onPostExecute(Object o) {

            this.ad.setMessage((String) o);
            this.ad.show();

        }

    }


    public void update(View view) {
        String usernom = this.txtnom.getText().toString();
        String userpre = this.txtprenom.getText().toString();

        String userN = this.txtuser.getText().toString();
        String userp = this.txtpw.getText().toString();
        String userold=this.txtolduser.getText().toString();
        dbWorker dbw = new dbWorker(this);
        dbw.execute(usernom, userpre,userN,userp,userold);
        Toast.makeText(this,"Successful update "+this.txtnom.getText(),Toast.LENGTH_LONG).show();


    }
    public void check(View view) {

    }
    public void participe(View view) {

    }



}
