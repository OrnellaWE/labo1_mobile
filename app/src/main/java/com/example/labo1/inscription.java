package com.example.labo1;

import android.app.AlertDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

public class inscription extends AppCompatActivity {
    private EditText txtnom;
    private EditText txtprenom;
    private EditText txtadresse;
    private EditText txttelephone;
    private EditText txtuser;
    private EditText txtpw;
    private Button sign;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.inscription);
        txtnom=(EditText)findViewById(R.id.nom);
        txtprenom=(EditText)findViewById(R.id.prenom);
        txtadresse=(EditText)findViewById(R.id.adresse);
        txttelephone=(EditText)findViewById(R.id.telephone);
        txtuser = (EditText) findViewById(R.id.usr);
        txtpw = (EditText) findViewById(R.id.pw);
        sign = (Button) findViewById(R.id.inscription);



    }
    public void inscription(View v) {

        String usernom = this.txtnom.getText().toString();
        String userpre = this.txtprenom.getText().toString();
        String userad = this.txtadresse.getText().toString();
        String usertel = this.txttelephone.getText().toString();
        String userN = this.txtuser.getText().toString();
        String userp = this.txtpw.getText().toString();

        class db extends AsyncTask {
            private Context c;
            private AlertDialog ad;

            public db(Context c) {
                this.c = c;
            }

            @Override
            protected void onPreExecute() {
                this.ad = new AlertDialog.Builder(this.c).create();
                this.ad.setTitle("Signing up");

            }



            @Override
            protected Object doInBackground(Object[] param) {
                String cible = "http://192.168.0.102/Android/inscription.php";
                try {
                    URL url = new URL(cible);
                    HttpURLConnection con = (HttpURLConnection) url.openConnection();
                    con.setDoInput(true);
                    con.setDoOutput(true);
                    con.setRequestMethod("POST");

                    OutputStream outs = con.getOutputStream();
                    BufferedWriter bufw = new BufferedWriter((new OutputStreamWriter(outs, "utf-8")));


                    String msg = URLEncoder.encode("nom", "utf-8")+"="+
                            URLEncoder.encode((String)param[0], "utf-8")+"&"+
                            URLEncoder.encode("prenom", "utf-8")+"="+
                            URLEncoder.encode((String)param[1], "utf-8")+"&"+
                            URLEncoder.encode("adresse", "utf-8")+"="+
                            URLEncoder.encode((String)param[2], "utf-8")+"&"+
                            URLEncoder.encode("telephone", "utf-8")+"="+
                            URLEncoder.encode((String)param[3], "utf-8")+"&"+
                            URLEncoder.encode("user", "utf-8")+"="+
                            URLEncoder.encode((String)param[4], "utf-8")+"&"+
                            URLEncoder.encode("pw", "utf-8")+"="+
                            URLEncoder.encode((String)param[5], "utf-8");

                    bufw.write(msg);
                    bufw.flush();
                    bufw.close();
                    outs.close();

                    InputStream ins = con.getInputStream();
                    BufferedReader bufr = new BufferedReader(new InputStreamReader(ins, "iso-8859-1"));
                    String line;
                    StringBuffer sbuff = new StringBuffer();

                    while ((line = bufr.readLine()) != null) {
                        sbuff.append(line + "\n");
                    }
                    return sbuff.toString();


                } catch (Exception e) {
                    return e.getMessage();

                }
            }

            @Override
            protected void onPostExecute(Object o) {
                this.ad.setMessage((String) o);
                this.ad.show();
            }
        }
        db dbw = new db(this);
        dbw.execute(usernom, userpre, userad, usertel, userN, userp);
        Intent i = new Intent(this, Menu.class);
        startActivity(i);
        Toast.makeText(this,"Welcome "+this.txtprenom.getText(),Toast.LENGTH_LONG).show();

    }


    public void connect(View view){
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);

    }
}
